#Importation
#############################################################

import Crypto,hashlib,os,sys,string
# from Crypto.Cipher import AES
# from Crypto.Cipher import ARC2
# from Crypto.Cipher import ARC4

import coinbase
from coinbase.wallet.client import Client

import requests
import json
import time
import datetime

#Graph
import matplotlib.pyplot as plt
import matplotlib
import numpy as np
from matplotlib.widgets import Slider

import seaborn
seaborn.set() ;

#Alert
import tkinter as tk
from tkinter.messagebox import*
import pygame

##############################################################

matplotlib.use('TkAgg');

####definition pour le hash
def encrypt(message):
	return hashlib.pbkdf2_hmac( "sha224" , message.encode() , b'salt' , 10000000 ).hex() ;

#Get Keys
##############################################################
with open('files/coinbaseKeys', 'a' , encoding="utf-8") as file:
    with open('files/coinbaseKeys', 'r') as file:
        text = file.read() ;
        file.close() ;

    file.close() ;

with open('files/coinbaseKeys', 'a' , encoding="utf-8") as file:
    if( text == '' ) :
        api_key =  ( input( "Enter your api key : " ) ) ;
        api_secret = ( input( "Enter your api secret key : " ) ) ;

        file.write( api_key + " " + api_secret ) ;

        file.close() ;

    with open('files/coinbaseKeys', 'r' , encoding="utf-8") as file:
        text = file.read() ;
        file.close() ;

    file.close() ;


keys = text.split( " " ) ;

api_key = keys[ 0 ] ;
api_secret = keys[ 1 ] ;

##############################################################


def playSong( audio ) :
	pygame.mixer.init() ;
	pygame.mixer.music.load( audio ) ;
	pygame.mixer.music.play( -1 ) ;

'''def ExitApp( dc ) :
	dc.destroy() ;
	pygame.mixer.music.stop() ;
'''
		
class Application( tk.Tk ) :
	def __init__( self , title , text ) :
		tk.Tk.__init__( self )
		self.create_widgets( title , text ) ;

	def create_widgets( self , title , text ) :
		self.label = tk.Label( self , text=text , pady=15 , padx=25 , font=( "", 14 ) ) ;
		self.label.grid( row=0 , columnspan=2 , column=0 )
		self.title( title ) ;

		self.grid( baseWidth=1 , baseHeight=1 , widthInc=1 , heightInc=90 ) ;
		self.bouton = tk.Button( self , text="OK" , command = self.ExitApp2 , pady=5 , padx=15 , fg="red" ) ;
		
		self.label.pack() ;
		self.bouton.pack() ;
		
		self.bind( "<Any-KeyPress>", self.ExitApp1 ) ;

	def ExitApp1( self , event ) :
		self.destroy() ;
		pygame.mixer.music.stop() ;

	def ExitApp2( self ) :
		self.destroy() ;
		pygame.mixer.music.stop() ;

class monitoring() :
    def __init__( self ) :
        self.client = None ;
        self.accounts = None ;
        self.user = None ;

        self.crypto = "" ;
        self.message = [] ;
        self.total = 0 ;
        self.currency = "" ;
        self.price = "" ;
        self.date = "" ;
        self.hour = "" ;
        self.minute = "" ;
        self.second = "" ;

        self.tabPrice = [] ;
        self.tabTime = [] ;

    #Calc total balance of account
    def getTotalBalance( self ):

        boucle = 0 ;

        while boucle == 0 :
            try :
                self.client = Client(api_key, api_secret ) ;
                self.accounts = self.client.get_accounts() ;
                self.user = self.client.get_current_user() ;
                boucle = 1 ;

            except coinbase.wallet.error.AuthenticationError:
                print( "reload..." ) ;
                time.sleep( 2 ) ;
                boucle = 0 ;

            except requests.exceptions.ConnectionError:
                print( "reload..." ) ;
                time.sleep( 2 ) ;
                boucle = 0 ;

            except simplejson.errors.JSONDecodeError:
                print( "reload..." ) ;
                time.sleep( 2 ) ;
                boucle = 0 ;
                
        self.message = [] ;
        
        total = 0 ;
        for wallet in self.accounts.data:
            value = str( wallet['native_balance']).replace('USD','');
	   	
            if( float( value ) != 0 ):
                self.message.append( 'Balance ' + str(wallet['currency']) + ' : ' + str(wallet['native_balance']) + ' => id :' + str(wallet['id']) );
                total += float(value);

        self.message.append( '\nTotal balance: ' + 'USD ' + str(total) ) ;
        self.total = total ;
        return total ;



    #Print total balance of account
    def printBalance( self ) :
        self.getTotalBalance() ;
        self.user = self.client.get_current_user() ;
        print( "\nname : " , self.user[ "name" ] ) ;
        print( "currency : " , self.user[ "native_currency" ] ) ;
        print( "\n---------------------------------------" ) ;
        print( ('\n').join( self.message ) );
        

    def getPrice( self , crypto , currency ) :
        self.crypto = crypto ;
        self.currency = currency ;

        boucle = 0 ;
        previous_price = 0.0 ;
        start_time = 0 ;
        currently_time = 0 ;
        times = 0 ;
        past_times = 0 ;
        previous_price = 0.0 ;
        fluctuation = 0.0 ;

        i = 0 ;

        while boucle == 0 :
            respons = requests.get( "https://api.coinbase.com/v2/prices/" + self.crypto + "-" + self.currency + "/spot" ) ;
			
            self.price = ( respons.json()[ 'data' ][ 'amount' ] ) ;
			
            self.date = datetime.datetime.now() ;
            self.hour = self.date.hour * 3600 ;
            self.minute = self.date.minute * 60 ;
            self.second = self.date.second ;

            plt.subplot() ;

            if( currently_time == 0 ) :
                times = 0 ;
                x = np.linspace( 0 , currently_time ) ;
                currently_time = self.hour + self.minute + self.second ;
                start_time = currently_time ;
                print( times , past_times ) ;
                print( currently_time );

            else :
                currently_time = self.hour + self.minute + self.second ;
                past_times = times ;
                times = currently_time - start_time ;
                x = np.linspace( past_times , times ) ;
                print( str(past_times) + "s - " + str(times) + "s" ) ;


            if( previous_price == 0.0 ):
                previous_price = float( self.price ) ;
				#print( previous_price ) ;
                fluctuation = float( self.price ) - previous_price ;

            else :
                fluctuation = float( self.price ) - previous_price ;


            print( self.date , " : " , self.price , " (", fluctuation , "$) ") ;

            y = float( self.price ) + x*0 ;

            plt.plot( x , y , "y") ;
            plt.pause(0.05 );
            #plt.grid() ;

            time.sleep( 5 );
            i = i + 5 ;

    def track( self , Xo , P , wallets ) :
        boucle = 0 ;
        pls = 0 ;
        loe = 5 ;
        Pa = ( ( ( 100 + P )*10000 ) / ( 100 - loe )**2 ) - 100 ;
        print( "\n\nYou need " + str(Pa) + "%\n\n" ) ;
        i = 0 ;
        start_time = 0 ;
        currently_time = 0 ;
        times = 0 ;
        past_times = 0 ;
        pause = 60 ;
        self.tabTime = [] ;
        self.tabPrice = [0] ;

        if( wallets == "all" ) :

            while boucle == 0 :
                self.date = datetime.datetime.now() ;
                self.hour = self.date.hour * 3600 ;
                self.minute = self.date.minute * 60 ;
                self.second = self.date.second ;
                    
                if( currently_time == 0 ) :
                    times = 0 ;
                    #x = np.linspace( 0 , currently_time ) ;
                    self.tabTime.append( 0 ) ;
                    self.tabTime.append( currently_time ) ;
                    
                    currently_time = self.hour + self.minute + self.second ;
                    start_time = currently_time ;
                    
                else:
                    currently_time = self.hour + self.minute + self.second ;
                    past_times = times ;
                    times = currently_time - start_time ;
                    #x = np.linspace( past_times , times ) ;
                    self.tabTime.append( times / 10 ) ;


                totalBalance = self.getTotalBalance() ;
                Pt = ( ( float(totalBalance) ) / ( Xo*0.000001*(100-loe)**2 ) ) - 100 ;
                
                y = float( Pt ) #+ x*0 ;
                self.tabPrice.append( y / 10 ) ;

                print( str(past_times) + "s to " + str(times) + "s => " + str(y) + "%\n" ) ;

                if self.tabPrice[ len(self.tabPrice) - 2 ] > self.tabPrice[ len(self.tabPrice) - 1 ] :
                    plt.plot( [ self.tabTime[ len(self.tabTime) - 2 ] , self.tabTime[ len(self.tabTime) - 1 ]  ] ,\
                        [ self.tabPrice[ len(self.tabPrice) - 2 ] , self.tabPrice[ len(self.tabPrice) - 1 ] ] , "y" ,\
                        marker="o", markersize=3 , color="red" ) ;
                    plt.text( self.tabTime[ len(self.tabTime) - 1 ] , self.tabPrice[ len(self.tabPrice) - 1 ] ,\
                        "{:.2f}%".format(self.tabPrice[ len(self.tabPrice) - 1 ] * 10  ) ) ;

                elif self.tabPrice[ len(self.tabPrice) - 2 ] < self.tabPrice[ len(self.tabPrice) - 1 ] :
                    plt.plot( [ self.tabTime[ len(self.tabTime) - 2 ] , self.tabTime[ len(self.tabTime) - 1 ]  ] ,\
                        [ self.tabPrice[ len(self.tabPrice) - 2 ] , self.tabPrice[ len(self.tabPrice) - 1 ] ] , "y" ,\
                        marker="o", markersize=3 , color="green" ) ;
                    plt.text( self.tabTime[ len(self.tabTime) - 1 ] , self.tabPrice[ len(self.tabPrice) - 1 ] ,\
                        "{:.2f}%".format(self.tabPrice[ len(self.tabPrice) - 1 ] * 10  ) ) ;

                else :
                    plt.plot( [ self.tabTime[ len(self.tabTime) - 2 ] , self.tabTime[ len(self.tabTime) - 1 ]  ] ,\
                        [ self.tabPrice[ len(self.tabPrice) - 2 ] , self.tabPrice[ len(self.tabPrice) - 1 ] ] , "y" ,  marker="o" , color="yellow" ) ;
                

                plt.xlim( self.tabTime[ len(self.tabTime) - 2 ] - pause  , times/10 + 1 ) ;
                plt.subplot() ;
                #Slider( plt.figure() , '' , i , self.tabTime[ len(self.tabTime) - 2 ] , valinit=0 ) ;

                #plt.grid() ;

                plt.pause( pause );

                if Pt >= Pa and pls == 0 :
                    playSong( "sound/beep.ogg" ) ;

                    app = Application( "Alert" , "Vous avez atteint les " + str(P) + "%" ) ;
                    app.mainloop() ;

                    pls = 1 ;

                time.sleep( pause ) ;
                i = i + 3 ;
                


monitoring = monitoring() ;

if( sys.argv[1] == "--track" and sys.argv[2] != "-b" )  :
	monitoring.getPrice( sys.argv[2] , sys.argv[3] ) ;

elif( sys.argv[1] == "--track" and sys.argv[2] == "-b" and sys.argv[3] != "" ) :
    Xo = int( str( sys.argv[3] ).split( "=" )[1] ) ;
    P = int( str( sys.argv[4] ).split( "=" )[1] ) ;
    wallets =  str( sys.argv[5].split( "=" )[1] ) ;

    monitoring.track( Xo , P , wallets ) ;
    
elif( sys.argv[1] == "balance" ) :
	monitoring.printBalance() ;

#txs = client.transfer_money( type="transfer" , account_id="61a1f355-f9c3-5f1d-a2ff-d5c94684fbd9" , to="666c2431-1b1f-5de3-8718-5d2f8e561c78" , amount="100" , currency="NU" , description="Important transfert") ;
#print( client["version"] ) ;